import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'ng-alertBox',
    templateUrl: './alertBox.component.html',
    styleUrls: ['./alertBox.component.scss']
})
export class AlerBoxComponent implements OnInit {

    @Input() title: string = '';

    @Input() message: string = '';

    @Input() mainActionLabel: string = '';

    @Input() secondaryActionLabel: string = '';

    @Input() icon: string = '';

    @Input() mainColor: string = '3f51b5';

    @Output() mainActionOutput = new EventEmitter<Event>();

    @Output() secondaryActionOutput = new EventEmitter<Event>();

    constructor() { }

    ngOnInit() { }

    public handleMainClick() {
        this.mainActionOutput.emit();
    }

    public handleSecondaryClick() {
        this.secondaryActionOutput.emit();
    }

    public getBackground() {
        return { background: `#${this.mainColor}` };
    }
}
