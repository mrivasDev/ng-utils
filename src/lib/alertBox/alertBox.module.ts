import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { AlerBoxComponent } from './alertBox.component';

@NgModule({
    declarations: [AlerBoxComponent],
    imports: [
        CommonModule,
        MatIconModule,
        MatButtonModule,
    ],
    exports: [
        AlerBoxComponent
    ]
})
export class AlertBoxModule { }
