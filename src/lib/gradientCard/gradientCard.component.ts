import { Component, Input } from '@angular/core';

export interface GradientCardConfig {
	icon?: string;
	title?: string;
	color?: string;
	imageUrl?:string;
	subtitle?: string;
	closable?: boolean;
	background?: string;
}

@Component({
  selector: 'ng-gradient-card',
  templateUrl: './gradientCard.component.html',
  styleUrls: ['./gradientCard.component.scss'],
})
export class GradientCardComponent {
  @Input() color: string = "#fff";
  @Input() background: string = "#60efbc";

  @Input() icon: string = "";
  @Input() imageUrl: string = "";

  @Input() title: string = "";
  @Input() subtitle: string = "";

  @Input() closable: boolean = true;

  visible: boolean = true;

  closeCard(): void {
    this.visible = false;
  }
}
