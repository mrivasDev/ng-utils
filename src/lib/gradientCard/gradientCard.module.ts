import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GradientCardComponent } from './gradientCard.component';

@NgModule({
  declarations: [
    GradientCardComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    GradientCardComponent
  ]
})
export class GradientCardModule { }
