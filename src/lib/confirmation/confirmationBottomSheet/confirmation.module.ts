import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmationMobileComponent } from './confirmation.component';
import { MatBottomSheetModule, MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import { ErrorAlertModule } from '../../alert/error-alert/error-alert.module';
import { ButtonModule } from '../../button/button.module';
import { LoadingModule } from '../../loading/loading.module';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [
    ConfirmationMobileComponent,
  ],
  imports: [
    CommonModule,
    MatIconModule,
    ButtonModule,
		MatButtonModule,
    LoadingModule,
    MatBottomSheetModule,
    ErrorAlertModule
  ],
  exports: [
    ConfirmationMobileComponent
  ],
  providers: [
    { provide: MatBottomSheetRef, useValue: {} },
    { provide: MAT_BOTTOM_SHEET_DATA, useValue: {} }
  ]
})
export class ConfirmationMobileModule { }
