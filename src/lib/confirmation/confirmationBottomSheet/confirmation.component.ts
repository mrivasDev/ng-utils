import { ChangeDetectorRef, Component, Inject, } from '@angular/core';
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import { HttpService, UtilService } from '../../../../../../src/app/core';

@Component({
	selector: 'ng-confirmation-mobile',
	templateUrl: './confirmation.component.html',
	styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationMobileComponent {

	public icon: string = '';
	public title: string = '';
	public subtitle: string = '';
	public apiUrl: string = '';
	public loading: boolean = false
	public element: any = null;
	public errorMessage: string = '';
	public method: string = '';

	constructor(
		private bottomSheetRef: MatBottomSheetRef<ConfirmationMobileComponent>,
		private httpService: HttpService,
		private utilService: UtilService,
		private _changeDetectorRef: ChangeDetectorRef,
		@Inject(MAT_BOTTOM_SHEET_DATA) public data: any
		) {
			this.title = data.title;
			this.subtitle = data.subtitle;
			this.apiUrl = data.apiUrl;
			this.icon = data.icon;
			this.element = data.element;
			if(data.method)
				this.method = data.method;
		}

		public delete(event) {
			let request: Promise<any>;

			if(this.method == 'delete'){
				request = this.httpService.delete(this.apiUrl);
			}else{
				request = this.httpService.post(this.apiUrl, this.element);
			}

			request.then(({ success, message }) => {
				this._changeDetectorRef.markForCheck();
				this.loading = false;
				if (success) {
					this.utilService.notification(message, '', 3);
					this.close({success, message});
				} else {
					console.error(message);
				}
			}).catch((error) => {
				this.loading = false;
				console.error(error);
			});
		}


		public close(response?: any): void {
			this.bottomSheetRef.dismiss(response);
		}
	}


