import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmationDesktopComponent } from './confirmation.component';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { ErrorAlertModule } from '../../alert/error-alert/error-alert.module';
import { ButtonModule } from '../../button/button.module';
import { LoadingModule } from '../../loading/loading.module';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogRef } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [
    ConfirmationDesktopComponent,
  ],
  imports: [
    CommonModule,
    MatIconModule,
    ButtonModule,
		MatButtonModule,
    LoadingModule,
    MatBottomSheetModule,
    ErrorAlertModule
  ],
  exports: [
    ConfirmationDesktopComponent
  ],
  providers: [
    { provide: MatDialogRef, useValue: {} },

  ]
})
export class ConfirmationDesktopModule { }
