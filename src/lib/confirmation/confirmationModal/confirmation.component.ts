import { Component, Inject, } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpService, UtilService } from '../../../../../../src/app/core';
import { MainActionModalComponent } from '../../mainActionModal/mainActionModal.component';

@Component({
  selector: 'ng-confirmation-desktop',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationDesktopComponent {

  public icon: string = '';
  public title: string = '';
  public subtitle: string = '';
  public apiUrl: string = '';
  public loading: boolean = false
  public element: any = null;
  public errorMessage: string = '';
  public method: string='';

  constructor(
    public dialogRef: MatDialogRef<MainActionModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private httpService: HttpService,
    private utilService: UtilService,

  ) {
    this.title = data.title;
    this.subtitle = data.subtitle;
    this.apiUrl = data.apiUrl;
    this.icon = data.icon;
    this.element = data.element;
    if(data.method){
      this.method = data.method;
    }
  }

  public delete(event) {
    let request: Promise<any>;

    if(this.method == 'delete'){
      request = this.httpService.delete(this.apiUrl);
    }else{
      request = this.httpService.post(this.apiUrl, this.element);
    }

    request.then(({ success, message }) => {
      this.loading = false;
      if (success) {
        this.utilService.notification(message, '', 3);
        this.close({success, message});
      } else {
        console.error(message);
      }
    }).catch((error) => {
      this.loading = false;
      console.error(error);
    });
  }


  public close(response?: any): void {
    this.dialogRef.close(response);
  }
}


