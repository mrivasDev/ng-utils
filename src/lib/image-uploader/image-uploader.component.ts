import { AfterViewInit, Component, EventEmitter, Input, Output, SimpleChanges, ViewChild } from '@angular/core';
import { HttpService, UtilService } from '../../../../../src/app/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { MatButtonToggleChange } from '@angular/material/button-toggle';
import { ElementRef } from '@angular/core';

interface ImageUploadEvent {
  error?: any,
  success: boolean,
  message: string,
  url?: string,
  id?: string
}

@Component({
  selector: 'ng-image-uploader',
  templateUrl: './image-uploader.component.html',
  styleUrls: ['./image-uploader.component.scss']
})
export class ImageUploaderComponent{

  @Input() entity: string = '';

  @Input() imageBase64: any = '';

  @Output() imageUploadEvent: EventEmitter<ImageUploadEvent> = new EventEmitter();

  @Output() imageUploaded: EventEmitter<boolean> = new EventEmitter();

  @ViewChild("uploader", { static: false }) uploader: ElementRef;

  public modalTitle: string = '';

  public element: any = null;

  public imageChangedEvent: any = '';

  public hasImage: boolean = false;

  public croppedImage: any = '';

  public file: File = null;

  public editionMode: 'read' | 'edit' = 'read';

  public editionControlMode = null;

  constructor( private httpService: HttpService, private utilService: UtilService ) {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.imageBase64 && !changes.imageBase64.previousValue && changes.imageBase64.currentValue){
      this.hasImage = true;
    }
  }

  public fileChangeEvent(event: any): void {
    this.editionMode = 'edit';
    this.imageChangedEvent = event;
    this.hasImage = true;
  }

  public handleModeChange({ value }: MatButtonToggleChange): void {
    this.editionMode = value;
  }

  public imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    this.imageUploaded.emit(true);
    this.file = this.utilService.base64ToFile(event.base64, this.imageChangedEvent.target?.files[0].name );
  }

  public loadImageFailed(event: any) {
    this.utilService.notification('El formato de la imagen no es correcto. Recuerde usar PNG/JPG');
  }

  public async loadImage(event: any) {
    event.preventDefault();
    this.imageUploaded.emit(true);
    this.uploader.nativeElement.click()
  }

  public async submit(): Promise<ImageUploadEvent> {
    if (this.file){
      const formData = new FormData();
      formData.append('file', this.file);
      const {id, preview} = await this.httpService.post(`upload/image/public/${this.entity}`, formData);
      if (id) {
        return ({
          success: true,
          message: 'La imagen se subio correctamente',
          url: preview,
          id: id
        })
      } else {
        return ({
          success: false,
          message: 'La imagen no se subio correctamente',
        })
      }
    }
  }
}
