import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { LoadingModule } from '../loading/loading.module';
import { ImageUploaderComponent } from './image-uploader.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatButtonToggleModule } from '@angular/material/button-toggle';

@NgModule({
  declarations: [ImageUploaderComponent],
  imports: [
    CommonModule,
    ImageCropperModule,
    MatButtonModule,
    LoadingModule,
    MatIconModule,
    MatSlideToggleModule,
    MatButtonToggleModule
  ],
  exports: [ImageUploaderComponent]
})
export class ImageUploaderModule { }
