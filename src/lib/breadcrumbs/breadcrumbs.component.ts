import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

interface Breadcrumb{
  label: string,
  link: string
}

@Component({
  selector: 'ng-breadcrumbs',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})

export class BreadcrumbsComponent implements OnInit{

  public crumbs: any = [];

  constructor(private router: Router) { }

  ngOnInit(){
    console.log(this.router.url);
    this.crumbs = this.router.url.split('/').filter(e=>e);
  }

}
