import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { LoadingModule } from '../loading/loading.module';
import { FileUploaderComponent } from './file-uploader.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { NgxFileDropModule } from 'ngx-file-drop';

@NgModule({
	declarations: [FileUploaderComponent],
	imports: [
		CommonModule,
		LoadingModule,
		FormsModule,
		ReactiveFormsModule,
		NgxFileDropModule,
		MatCardModule,
		MatIconModule,
		MatExpansionModule,
		MatButtonModule,
		MatFormFieldModule,
		MatProgressBarModule,
		MatInputModule

	],
	exports: [FileUploaderComponent]
})
export class FileUploaderModule { }
