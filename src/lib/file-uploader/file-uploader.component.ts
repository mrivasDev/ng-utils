

import {
	Component,
	ViewChild,
	Input,
	ViewEncapsulation,
	OnInit,
	ChangeDetectorRef,
	Output,
	EventEmitter,
} from '@angular/core';
import { FileSystemDirectoryEntry, FileSystemFileEntry, NgxFileDropComponent, NgxFileDropEntry, NgxFileDropModule } from 'ngx-file-drop';
import { HttpService, UtilService } from '../../../../../src/app/core';
import {
	FormGroup,
	FormControl,
	ValidationErrors,
	ValidatorFn,
	Validators,
} from '@angular/forms';
import { HttpEvent, HttpEventType } from '@angular/common/http';

const NO_FILE = '';
const NO_VIEW_FILE = 'No se cargó ningún archivo';

interface Upload {
	id: number;
	hash: string;
	type: string;
	name: string;
	folder: string;
	public: boolean;
	created_at: string;
	preview?: string;
}

@Component({
	selector: 'ng-file-uploader',
	templateUrl: './file-uploader.component.html',
	styleUrls: ['./file-uploader.component.scss']
})

export class FileUploaderComponent implements OnInit {
	@ViewChild(NgxFileDropComponent) fileComponent: NgxFileDropComponent;

	public documents: any[] = [];

	public fileName: FormControl = new FormControl(NO_FILE);

	@Input() extensiones:string;

	@Input() title = 'Adjuntar archivos';

	@Input() expended = false;

	@Input() formElementName: string;

	@Input() form: FormGroup;

	@Input() endpoint: string;

	@Input() multiple: boolean;

	@Input() disabled = false;

	@Input() view: string;

	@Input() document: any;

	@Input() messages = {
		openFile: 'Abrir archivos',
		noFile: 'No hay archivos',
		uploadedFile: 'Archivos subidos',
		uploadFile: 'Adjuntar archivos',
	};

	@Input() files:any[]=[];

	@Input() requiredErrorLabel: string = 'The document is required';

	@Output() previewChange = new EventEmitter();

	@Output() linkPreview = new EventEmitter();

	@Output() fileNameEmitter = new EventEmitter();

	@Output() fileIdEmitter = new EventEmitter<number>();

	@Output() removeIdEmitter = new EventEmitter<number>();

	viewMode = false;
	loading = null;
	loadingMode = 'determinate';
	link: string;

	requiredFile: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
		const resultado = this.form.get(this.formElementName).value
			? null
			: { requiredFile: true };
		return resultado;
	}

	constructor(
		private utilService: UtilService,
		private httpService: HttpService,
		private changeDetector: ChangeDetectorRef
	) { }

	ngOnInit() {
		if (this.form && this.form.get(this.formElementName)) {
			this.form.get(this.formElementName).valueChanges.subscribe((val) => {
				if (!val) {
					this.reset();
				}
			});
			if (this.form.get(this.formElementName).value) {
				this.getUpload();
			}
		}
	}

	dropped(uploadEvent: any[]) {
		//this.documents = uploadEvent;
		if (this.disabled) {
			return;
		}
		let alert = false;
		for (const droppedFile of uploadEvent) {
			if (droppedFile.fileEntry.isFile) {
				const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
				const extension: string = fileEntry.name.substring(
					fileEntry.name.lastIndexOf('.') + 1,
					fileEntry.name.length
				);
				if(!this.extensiones.includes(extension.toLowerCase())){
					if(!alert){
						this.utilService.notification(
							`El archivo "${fileEntry.name}" no es válido. Ingrese un archivo con
							alguna de estas extensiones: ${this.extensiones.split(',').join(', ')}`
						);
						alert=true;
					}
				} else {
					fileEntry.file((file: File) => {
						this.loading = 0;
						this.loadingMode = 'determinate';
						const fileSub = this.httpService
							.upload(`upload/document/private/${this.endpoint}`, file)
							.subscribe(
								(event: HttpEvent<any>) => {
									if (event.type === HttpEventType.UploadProgress) {
										this.loading = Math.round(
											(100 * event.loaded) / event.total
										);
										if (this.loading === 100) {
											this.loadingMode = 'indeterminate';
										}
									} else if (event.type === HttpEventType.Response) {
										this.loading = null;
										this.loadingMode = 'indeterminate';
										const res = event.body;
										let doc={
											'upload_id':res.id,
											'name':res.name,
											'preview':res.preview,
											'id':res.id
										};
										this.documents.push(doc);
										if (this.form && this.form.get(this.formElementName)) {
											this.form
												.get(this.formElementName)
												.setValue(this.documents[0].id);
										}
										this.fileName.setValue(fileEntry.name);
										this.fileNameEmitter.emit(this.fileName.value);
										this.link = res.preview;
										this.linkPreview.emit(this.link);
										this.previewChange.emit(res.preview);
										this.fileIdEmitter.emit(res.id);

										if(this.multiple)
											this.loading=null;

										fileSub.unsubscribe();
									}
								},
								(err) => {
									console.error(err);
									this.reset(null);
									this.utilService.notification('No se pudo subir el archivo');
								}
							);
					});
				}
			}
		}
		this.fileComponent.fileSelector.nativeElement.value = [];
	}

	onClickButton($event?: MouseEvent) {
		if (this.disabled) {
			return;
		}
		if (this.loading !== null) {
			this.reset(null);
		} else{
			if(!this.multiple){
				if (this.documents.length === 0) {
					if (this.fileComponent) {
		
						this.fileComponent.openFileSelector($event);
					}
				}
			}else{
				if(this.fileComponent){
					this.fileComponent.openFileSelector($event);
				}
			}
		}
	}

	onClickInput() {
		if (this.documents.length === 0 && this.loading === null) {
			this.onClickButton();
		}
	}

	reset(event?) {
		if (event) {
			event.preventDefault();
			event.stopPropagation();
		}
		this.fileName.setValue(NO_FILE);
		this.loading = null;
		this.link = null;
		this.previewChange.emit(null);
		this.loadingMode = 'determinate';
		this.documents = [];
		this.form.get(this.formElementName).reset(null, { emitEvent: false });
		this.fileName.markAsDirty();
		this.fileName.markAsTouched();
		this.fileName.updateValueAndValidity();
		this.linkPreview.emit(null);
	}

	setDisabled(disabled: boolean) {
		this.disabled = disabled;
		disabled ? this.fileName.disable() : this.fileName.enable();
		this.changeDetector.detectChanges();
	}

	open() {
		if (this.link) {
			window.open(this.link);
		}
	}

	setValidation() {
		const emptyControl = Object.assign({}, this.form.get(this.formElementName));
		(emptyControl as any).value = null;
		if (
			this.form.get(this.formElementName).validator &&
			'required' in (this.form.get(this.formElementName).validator(emptyControl) || {})
		) {
			this.fileName.setValidators([
				Validators.required,
				this.requiredFile,
			]);
			this.fileName.markAsDirty();
			this.fileName.markAsTouched();
			this.fileName.updateValueAndValidity();
		} else {
			this.fileName.setValidators(null);
			this.fileName.markAsPristine();
			this.fileName.markAsUntouched();
			this.fileName.updateValueAndValidity();
		}
	}

	getUpload() {
		this.httpService
			.get('upload/' + this.form.get(this.formElementName).value)
			.then((upload: Upload) => {
				this.documents = [upload];
				this.fileName.setValue(upload.name);
				this.link = upload.preview;
				this.previewChange.emit(upload.preview);
			});
	}

	getStyle() {
		const style = this.link
			? {
				backgroundImage: `url(${this.link})`,
				backgroundPosition: 'center',
				backgroundSize: 'cover',
			}
			: '';
		return style;
	}

	addToDocuments(file: any){
		this.documents.push(file);
		return this.documents;
	}

	removeElement(arr:string, index:number, fileId:number){
		this.httpService.delete(`advicesxUploads/deleteFile/${fileId}`).then((res:any)=>{
			if(res.success){
				arr=='documents'?this.documents.splice(index,1):this.files.splice(index,1);
				this.utilService.notification(res.message);
				this.removeIdEmitter.emit(fileId);
			}else{
				console.error(res.message);
			}			
		});
	}
}
