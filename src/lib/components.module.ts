import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from './button/button.module';
import { TableModule } from './table/table.module';
import { LoadingModule } from './loading/loading.module';
import { HeaderModule } from './header/header.module';
import { SegmentModule } from './segment/segment.module';
import { SuccessAlertModule } from './alert/success-alert/succes-alert.module';
import { ErrorAlertModule } from './alert/error-alert/error-alert.module';
import { GradientCardModule } from './gradientCard/gradientCard.module';
import { ImageUploaderModule } from './image-uploader/image-uploader.module';
import { AutocompleteModule } from './autocomplete/autocomplete.module';
import { AlertBoxModule } from './alertBox/alertBox.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TableModule,
    LoadingModule,
    HeaderModule,
    ButtonModule,
    SegmentModule,
    SuccessAlertModule,
    ErrorAlertModule,
    GradientCardModule,
    ImageUploaderModule,
		AutocompleteModule,
    AlertBoxModule,
  ],
  exports: [
    TableModule,
    LoadingModule,
    HeaderModule,
    ButtonModule,
    SegmentModule,
    SuccessAlertModule,
    ErrorAlertModule,
    GradientCardModule,
    ImageUploaderModule,
		AutocompleteModule,
    AlertBoxModule
  ],
  providers: [

  ]
})
export class ComponentsModule { }
