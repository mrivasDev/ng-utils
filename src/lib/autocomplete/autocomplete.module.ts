import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutocompleteComponent } from './autocomplete.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [AutocompleteComponent],
  imports: [
    CommonModule,
		MatFormFieldModule,
		MatInputModule,
		MatIconModule,
		MatProgressBarModule,
		MatAutocompleteModule,
		ReactiveFormsModule,
		MatButtonModule,
		MatCardModule
  ],
	exports: [
		AutocompleteComponent
	]
})
export class AutocompleteModule { }
