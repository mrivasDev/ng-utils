import { Component, EventEmitter, Injectable, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpService } from '../../../../../src/app/core';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';

@Component({
	selector: 'ng-autocomplete',
	templateUrl: './autocomplete.component.html',
	styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent implements OnInit {

	@Input() entity: string = '';

	@Input() label: string = 'Start your search here';

	@Input() placeholder: string = 'Search ANYTHING';

	@Input() loadingLabel: string = 'Loading . . .';

	@Input() searchingLabel: string = 'Selected Value: ';

	@Input() noResultsLabel: string = 'No results where found';

	@Input() displayKey: string = 'fullName';

	@Input() valueKey: string = 'id';

	@Input() mobile: boolean = false;

	@Output() onSelect = new EventEmitter<Event>();

	public searchControl: any = new FormControl();

	public filteredValues: any[] = [];

	public isLoading: boolean = false;

	public errorMsg: string = '';

	public showMobileSearch: boolean = false;

	public selected: boolean = false;

	constructor( private httpService: HttpService ) { }

	ngOnInit() {
		this.searchControl
		.valueChanges
		.pipe(debounceTime(500),tap(() => { this.errorMsg = ""; this.filteredValues = []; this.isLoading = true; }),
		switchMap(value => this.httpService.autocomplete(`${this.entity}`, {text: value})
		.pipe(finalize(() => { this.isLoading = false; }))))
		.subscribe((data: any[]) => {
			if (!data.length) {
				this.filteredValues = [];
				if (this.selected) { 
					this.errorMsg = '';
				} else {
					this.errorMsg = this.noResultsLabel;
				}
			} else {
				this.errorMsg = "";
				this.filteredValues = data;
			}
		});
	}

	public handleSelect(event: MatAutocompleteSelectedEvent) {
		this.onSelect.emit(event.option.value);
		this.selected = event.option.value;
	}

	public displayFn(element: any): string {
		if (!element){
			return '';
		}
		if (element && element.fullName){
			return element.fullName;
		}
	}

	public cleanSearch() {
		this.searchControl.reset();
	}

	public getElementValue(element: any) {
		return element[this.valueKey];
	}
	public getElementResult(element: any){
		return element[this.displayKey];
	}

	public toggleShowMobileSearch(){
		this.showMobileSearch = !this.showMobileSearch;
	}
}
