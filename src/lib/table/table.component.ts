import { Component, Input, Output, ViewChild } from '@angular/core';
import { MatPaginator, MatPaginatorIntl, PageEvent } from '@angular/material/paginator';
import { EventEmitter } from '@angular/core';

interface SearchMode {
  mode: 'local' | 'api';
}

export interface ActionTable {
  icon: string,
  name: string,
  callback: any
}

export interface MobileSource {
  id: number,
  title: string,
  description: string,
  expandedInfo: string[]
}

@Component({
  selector: 'ng-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent {

  public noElements: boolean = false;

  @Input() isLoading: boolean = false;

  @Input() mobile: boolean = false;

  @Input() dataSource: any[] = [];

  @Input() mobileSource: MobileSource[] = [];

  @Input() columnsToDisplay: { field: string, display: string }[] = [];

  @Input() mobileMainColumn: string = '';

  @Input() mobileSecondaryColumn: string = '';

  @Input() mobileExtraInformation: string = '';

  @Input() searchMode: SearchMode = { mode: 'local' };

  @Input() pageSize: number = 1;

  @Input() pageIndex: number = 0;

  @Input() pageSizeOptions: number[] = [5, 10, 15];

  @Input() resultsLength: number = 0;

  @Input() actions: ActionTable[] = [];

  @Output() paginationEvent = new EventEmitter<PageEvent>();

  @Output() segmentButtonEvent = new EventEmitter();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  public filter: any = null;

  public searchValue: string = '';

  constructor(private paginatorIntl: MatPaginatorIntl) {
    this.paginatorIntl.itemsPerPageLabel = 'Mostrar';
    this.paginatorIntl.nextPageLabel = 'Página siguiente';
    this.paginatorIntl.previousPageLabel = 'Página anterior';
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.searchValue = filterValue;

    if (filterValue.length > 3) {
      if (this.searchMode.mode === 'local') {
        //
      }
    } else {
      if (!filterValue) {
        //
      }
    }
  }

  getColumns(): string[] {
    return this.columnsToDisplay.map(column => column.field);
  }

  getColumnName(column: string): string {
    return this.columnsToDisplay.find(col => col.field === column).display;
  }


  handlePageEvent(event: PageEvent): void {
    this.paginationEvent.emit(event);
  }

  trackByFn(index, item) { if (!item) { return null; } return item.id; }

  segmentButton(event){
    this.segmentButtonEvent.emit();
  }
}
