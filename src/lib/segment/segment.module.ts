import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SegmentComponent } from './segment.component';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatRippleModule } from '@angular/material/core';
import { ButtonModule } from '../button/button.module';

@NgModule({
  declarations: [
    SegmentComponent,
  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatIconModule,
    MatRippleModule,
    ButtonModule
  ],
  exports: [
    SegmentComponent
  ]
})
export class SegmentModule { }
