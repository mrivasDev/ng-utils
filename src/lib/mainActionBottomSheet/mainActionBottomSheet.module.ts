import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';

import { ButtonModule } from '../button/button.module';
import { MainActionBottomSheetComponent } from './mainActionBottomSheet.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { ReactiveFormsModule } from '@angular/forms';
import { LoadingModule } from '../loading/loading.module';
import { MatBottomSheetModule, MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import { ErrorAlertModule } from '../alert/error-alert/error-alert.module';
import { ImageCropperModule } from 'ngx-image-cropper';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ImageUploaderModule } from '../image-uploader/image-uploader.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { FileUploaderModule } from '../file-uploader/file-uploader.module';

@NgModule({
    declarations: [
        MainActionBottomSheetComponent,
    ],
    imports: [
        CommonModule,
        ButtonModule,
        LoadingModule,
        MatBottomSheetModule,
        MatDividerModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatIconModule,
        MatButtonModule,
        MatRippleModule,
        MatFormFieldModule,
        MatCheckboxModule,
        MatSelectModule,
        MatInputModule,
        MatRadioModule,
        ReactiveFormsModule,
        ErrorAlertModule,
        ImageCropperModule,
        MatSlideToggleModule,
        ImageUploaderModule,
        AngularEditorModule,
				FileUploaderModule
    ],
    exports: [
        MainActionBottomSheetComponent
    ],
    providers: [
        MatDatepickerModule,
        { provide: MatBottomSheetRef, useValue: {} },
        { provide: MAT_BOTTOM_SHEET_DATA, useValue: {} }
    ]
})
export class MainActionBottomSheetModule { }
