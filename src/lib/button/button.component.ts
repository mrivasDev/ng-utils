import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UtilService } from '../../../../../src/app/core';
import { DashboardService } from '../../../../../src/app/pages/dashboard';

@Component({
	selector: 'ng-button',
	templateUrl: './button.component.html',
	styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

	@Input() primary: boolean = false;

	@Input() label: string = 'Button';

	@Input() icon: string = '';

	@Input() disabled: boolean = false;

	@Output() onClick = new EventEmitter<Event>();

	public themeColor: 'primary' | 'accent' = 'primary';

	constructor(
		private dashboardService: DashboardService,
		private utilService: UtilService
	) {
		this.themeColor = this.utilService.getLS('themeColor', true)
			? this.utilService.getLS('themeColor', true)
			: 'primary';
	}
	ngOnInit(): void {
		this.dashboardService.onPrimaryThemeSelected.subscribe(() => {
			this.themeColor = 'primary';
		});

		this.dashboardService.onAccentThemeSelected.subscribe(() => {
			this.themeColor = 'accent';
		});
	}
}
