import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingComponent } from './loading.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';


@NgModule({
    declarations: [
        LoadingComponent,
    ],
    imports: [
        CommonModule,
        MatProgressSpinnerModule,
        MatProgressBarModule
    ],
    exports: [
        LoadingComponent
    ]
})
export class LoadingModule { }
