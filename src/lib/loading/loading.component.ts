import { Component, Input } from '@angular/core';

@Component({
  selector: 'ng-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent {

  @Input() label: string = '';

  @Input() color: string = 'black';

	@Input() type: 'spinner' | 'bar' = 'spinner';

	@Input() progress: number = 100;

	@Input() loadingMode: 'determinate' | 'indeterminate' | 'buffer' | 'query' = 'indeterminate';

  constructor() { }

}
