import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Location } from '@angular/common'
import { MatDialog } from '@angular/material/dialog';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MainActionModalComponent } from '../mainActionModal/mainActionModal.component';
import { MainActionBottomSheetComponent } from '../mainActionBottomSheet/mainActionBottomSheet.component';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { ViewsFAQ } from '../../../../../src/app/shared/constants';
import { stringify } from 'querystring';
import {  Router } from '@angular/router';

export interface FormObject {
	field: string,
	display: string,
	type?: string,
	element: 'input' | 'textarea' | 'datepicker' | 'checkbox' | 'select' | 'radio' | 'file' | 'image' | 'wysiwyg' | 'file' | 'label',
	placeholder: string,
	options?: any[],
	class?: string,
	min?: number,
	max?: number,
	validator?: any
	defaultValue?: any,
	disabled?:boolean
}

export interface FormLayout {
	rowIndex: number,
	element: FormObject[]
}

@Component({
	selector: 'ng-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

	@Input() icon: string = '';
	@Input() title: string = 'Title of the screen';
	@Input() subtitle: string = 'SubTitle';

	@Input() titleClass: string = '';
	@Input() subtitleClass: string = '';

	@Input() mainActionLabel: string = '';
	@Input() mainActionPermission: boolean = true;
	@Input() secondaryActionLabel: string = '';
	@Input() secondaryActionPermission: boolean = true;

	@Output() mainActionEvent = new EventEmitter<Event>();
	@Output() secondaryActionEvent = new EventEmitter<Event>();
	@Output() infoActionEvent = new EventEmitter<Event>();

	@Input() modalTitle: string = '';
	@Input() modalSubTitle: string = '';

	@Input() modalApiUrl: string = '';
	@Input() modalForm: FormLayout[] = [];
	@Input() method: string = 'post';
	@Input() subUrl: string = '/edit';

	@Input() secondaryModalTitle: string = '';
	@Input() secondaryModalSubTitle: string = '';

	@Input() secondaryModalApiUrl: string = '';
	@Input() secondaryModalForm: FormLayout[] = [];
	@Input() secondaryMethod: string = 'post';

	//New Variables
	@Input() infoModalTitle: string = '';
	@Input() infoModalSubTitle: string = '';
	@Input() infoModalApiUrl: string = '';
	@Input() infoModalForm: FormLayout[] = [];
	@Input() infoModalMethod: string = 'post';
	@Input() userTypes:string[]=[];
	@Input() infoDataElement: any = null;

	@Input() mobile: boolean = false;

	@Input() angularEditorConfig: AngularEditorConfig = null;

	@Output() actionFinished = new EventEmitter<Event>();
	@Output() onChangeSelect = new EventEmitter<any>();

	@Input() marginButton: boolean = true;

	@Input() backButton: boolean = false;

	public faqMap: Map<string,number> = new Map<string,number>([
		['/secretaria-tecnica',ViewsFAQ.TECHNICAL_SECRETARY]
	]);

	constructor(
		public mainActionDialog: MatDialog,
		private mainActionBottomSheet: MatBottomSheet,
		public secondaryActionDialog: MatDialog,
		private secondaryActionBottomSheet: MatBottomSheet,
		private infoActionDialog: MatDialog,
		private infoActionBottomSheet: MatBottomSheet,
		private location: Location,
		private router: Router) { }

	ngOnInit(): void { }

	public mainClickHandler() {
		if (this.mainActionEvent.observers.length) {
			this.mainActionEvent.emit();
		} else {
			if (this.mobile) {
				this.openMainBottomSheet();
			} else {
				this.openMainDialog();
			}
		}
	}

	public openMainDialog(): void {
		const dialogRef = this.mainActionDialog.open(MainActionModalComponent, {
			width: '650px',
			height: 'auto',
			data: {
				title: this.modalTitle,
				subtitle: this.modalSubTitle,
				apiUrl: this.modalApiUrl,
				form: this.modalForm,
				method: this.method,
				subUrl: this.subUrl,
				angularEditorConfig: this.angularEditorConfig,
				multiple: this.router.url.includes('asesoramiento-contable')
			}
		});

		//Escuchar eventos onchange en los selects object = (field, value)
		dialogRef.componentInstance.onChangeSelect
		.subscribe( object =>{
			 this.onChangeSelect.emit(object)
			});
		
		dialogRef.afterClosed().subscribe(() => {
			this.actionFinished.emit();
		});
	}

	public openMainBottomSheet(): void {
		const bottomSheet = this.mainActionBottomSheet.open(MainActionBottomSheetComponent, {
			data: {
				title: this.modalTitle,
				subtitle: this.modalSubTitle,
				apiUrl: this.modalApiUrl,
				form: this.modalForm,
				method: this.method,
				subUrl: this.subUrl,
				angularEditorConfig: this.angularEditorConfig,
				multiple: this.router.url.includes('asesoramiento-contable')
			}
		});
		//listen onChange events from select object=(field,value)
		bottomSheet.instance.onChangeSelect.subscribe(object=>{
			this.onChangeSelect.emit(object);
		});
		bottomSheet.afterDismissed().subscribe(() => {
			this.actionFinished.emit();
		});
	}

	public secondaryClickHandler() {
		if (this.secondaryActionEvent.observers.length) {
			this.secondaryActionEvent.emit();
		} else {
			if (this.mobile) {
				this.openSecondaryBottomSheet();
			} else {
				this.openSecondaryDialog();
			}
		}
	}

	public openSecondaryDialog(): void {
		const dialogRef = this.secondaryActionDialog.open(MainActionModalComponent, {
			width: '650px',
			height: 'auto',
			data: {
				title: this.secondaryModalTitle,
				subtitle: this.secondaryModalSubTitle,
				apiUrl: this.secondaryModalApiUrl,
				form: this.secondaryModalForm,
				method: this.secondaryMethod,
				angularEditorConfig: this.angularEditorConfig,
				multiple: this.router.url.includes('asesoramiento-contable')
			}
		});

		dialogRef.afterClosed().subscribe(() => {
			this.actionFinished.emit();
		});
	}

	public openSecondaryBottomSheet(): void {
		const bottomSheet = this.secondaryActionBottomSheet.open(MainActionBottomSheetComponent, {
			data: {
				title: this.secondaryModalTitle,
				subtitle: this.secondaryModalSubTitle,
				apiUrl: this.secondaryModalApiUrl,
				form: this.secondaryModalForm,
				method: this.secondaryMethod,
				angularEditorConfig: this.angularEditorConfig,
				multiple: this.router.url.includes('asesoramiento-contable')
			}
		});
		bottomSheet.afterDismissed().subscribe(() => {
			this.actionFinished.emit();
		});
	}

	public infoButtonHandler() {
		this.setClickedButtonInLocalStorage();
		if (this.infoActionEvent.observers.length) {
			this.infoActionEvent.emit();
		} else {
			if (this.mobile) {
				this.openInfoBottomSheet();
			} else {
				this.openInfoDialog();
			}
		}
	}

	public openInfoBottomSheet(): void {
		const bottomSheet = this.infoActionBottomSheet.open(MainActionBottomSheetComponent, {
			data: {
				title: this.infoModalTitle,
				subtitle: this.infoModalSubTitle,
				apiUrl: this.infoModalApiUrl,
				form: this.infoModalForm,
				method: this.infoModalMethod,
				element: this.infoDataElement,
				angularEditorConfig: this.angularEditorConfig,
				showElement:this.isGerenciaOrAdmin(),
				isFAQ:true
			}
		});
		bottomSheet.afterDismissed().subscribe(() => {
			this.actionFinished.emit();
		});
	}

	public openInfoDialog(): void {
		const dialogRef = this.infoActionDialog.open(MainActionModalComponent, {
			width: '800px',
			height: 'auto',
			data: {
				title: this.infoModalTitle,
				subtitle: this.infoModalSubTitle,
				apiUrl: this.infoModalApiUrl,
				form: this.infoModalForm,
				method: this.infoModalMethod,
				element: this.infoDataElement,
				angularEditorConfig: this.angularEditorConfig,
				showElement:this.isGerenciaOrAdmin(),
				isFAQ:true
			}
		});

		dialogRef.afterClosed().subscribe(() => {
			this.actionFinished.emit();
		});
	}

	public goBack(): void {
		this.location.back()
	}

	public isGerenciaOrAdmin():boolean{
		return this.userTypes.some(el=>el=='gerencia' || el=='admin');
	}

	setClickedButtonInLocalStorage(){
		//Obtenemos el item de localstorage
		let viewedFAQs = JSON.parse(localStorage.getItem("viewedFAQs")) as number[] ? JSON.parse(localStorage.getItem("viewedFAQs")) as number[] : [];
		//Usamos el map para obtener el id segun la URL
		let faqId = this.faqMap.get(this.router.url);
		//Si no existe el elemento lo pusheamos
		if(!viewedFAQs.some(e => e == faqId))
			viewedFAQs.push(faqId);
		localStorage.setItem("viewedFAQs",JSON.stringify(viewedFAQs));
	}

	setInfoButtonColor(){
		//Obtenemos el item de localstorage
		let viewedFAQs = JSON.parse(localStorage.getItem("viewedFAQs")) as number[] ? JSON.parse(localStorage.getItem("viewedFAQs")) as number[] : [];
		//Usamos el map para obtener el id segun la URL
		let faqId = this.faqMap.get(this.router.url);
		if(JSON.parse(localStorage.getItem('isDarkTheme')) && viewedFAQs.some(e => e == faqId)){
			return 'viewed-dark';
		}else if(!JSON.parse(localStorage.getItem('isDarkTheme')) && viewedFAQs.some(e => e == faqId)){
				return 'viewed';
		}else{
			return 'not-viewed';
		}
	}
}