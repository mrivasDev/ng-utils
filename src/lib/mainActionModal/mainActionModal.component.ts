import { Component, ElementRef, EventEmitter, Inject, Output, Renderer2, ViewChild, } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpService, UtilService } from '../../../../../src/app/core';
import { FormLayout } from '../header/header.component';
import { ImageUploaderComponent } from '../image-uploader/image-uploader.component';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { FileUploaderComponent } from '../file-uploader/file-uploader.component';

@Component({
	selector: 'ng-main-action-modal',
	templateUrl: './mainActionModal.component.html',
	styleUrls: ['./mainActionModal.component.scss']
})

export class MainActionModalComponent {
	@ViewChild(FileUploaderComponent) fileUploaderComponent: FileUploaderComponent;
	//Primera posicion "field" del form y segunda posicion el "value".
	@Output() onChangeSelect = new EventEmitter<any>();
	public dataElement: any = null;
	public title: string = '';
	public subtitle: string = '';
	public apiUrl: string = '';
	public form: any[] = [];
	public layout: FormLayout[] = [];
	public formGroup: any = null;
	public method: string = '';
	public errorMessage: string = '';
	public loading: boolean = false;

	private hasImage: boolean = false;
	public subUrl: string = '/edit/';
	public file: any = null;
	private imageUrl: string = '';

	public saveLabel: string = 'Guardar';

	public angularEditorConfig: AngularEditorConfig = null;
	//New variable
	public showElement:boolean=true;
	//New array
	public filesId:number[]=[];
	public uploadedFilesId:number[]=[];
	public files:any[]=[];
	public multiple: boolean = false;
	public supportedExtensions:string='txt,doc,docx,jpg,jpeg,png,pdf,xls,xlsx';
	public isFAQ:boolean=false;
	public isOnlyDisplayInfo:boolean=false;
	public displayElements:any;
	// new User Variables
	public isUserForm: boolean = false;
	public isActive: boolean = false;
	//new Emitter
	@Output() idEmitter=new EventEmitter<number>();

	@ViewChild(ImageUploaderComponent) imageUploaderComponent: ImageUploaderComponent;

	constructor(
		public dialogRef: MatDialogRef<MainActionModalComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private utilService: UtilService,
		private httpService: HttpService,
		private sanitizer:DomSanitizer,
	) {
		this.title = data.title;
		this.subtitle = data.subtitle;
		this.apiUrl = data.apiUrl;
		this.layout = data.form;

		if (data.saveLabel) {
			this.saveLabel = data.saveLabel;
		}

		if (data.angularEditorConfig) {
			this.angularEditorConfig = data.angularEditorConfig;
		}

		if (data.element) {
			this.dataElement = data.element;
		}

		if(data.showElement!=undefined){
			this.showElement=data.showElement;
		}

		if(data.files){
			this.getUploads(data.files);
		}

		if(data.multiple){
			this.multiple=data.multiple;
		}

		if(data.apiUrl=='advices'){
			this.supportedExtensions='pdf';
		}

		if(data.isFAQ){
			this.isFAQ=data.isFAQ;
		}

		if(data.isOnlyDisplayInfo){
			this.isOnlyDisplayInfo=data.isOnlyDisplayInfo;
			if(data.displayElements){
				this.displayElements=data.displayElements;
			}
		}
		
		if(data.form){
			this.form = data.form.map(({ element }) => (
				element.flat()
			)).flat();
		}
		
		if(data.isUserForm) {
			this.isUserForm = data.isUserForm;
		}

		if(data.isActive) {
			this.isActive = data.isActive;
		}
		this.method = data.method;
		this.subUrl = data.subUrl || '/edit/';
		this.initForm();
	}

	private async initForm() {
		const formObj = this.form.reduce((accumulatedElement, {
			field, validator, defaultValue,disabled
		}) => {
			let formValidator = null;
			if (validator) {
				if (typeof validator !== 'function') {
					formValidator = [];
					validator.forEach(validation => {
						formValidator.push(validation);
					});
					accumulatedElement[field] = new FormControl({value : defaultValue ? defaultValue : null,disabled:disabled}, formValidator);
				} else {
					accumulatedElement[field] = new FormControl({value : defaultValue ? defaultValue : null,disabled:disabled}, validator);
				}
			} else {
				accumulatedElement[field] = new FormControl({value : defaultValue ? defaultValue : null,disabled:disabled});
			}
			return accumulatedElement;
		}, {});
		this.formGroup = new FormGroup(formObj);
		if (this.dataElement) {
			if (this.dataElement.image) {
				this.file = await this.utilService.getBase64FromUrl(this.dataElement.image);
			}
			this.formGroup.patchValue(this.dataElement);
		}
	}

	public getUploads(files:any[]){
		files.forEach(file=>{
			this.httpService
			.get(`upload/${file.upload_id}`)
			.then((upload: any) => {
				let f={
					'upload_id':upload.id,
					'preview': upload.preview,
					'name': upload.name,
					'id':file.id
				};
				if(this.method==='put'){
					if(this.multiple){
						this.uploadedFilesId.push(file.upload_id);
						this.formGroup.get('upload_id').setValue(this.fileUploaderComponent.addToDocuments(f));
					}
				}else{
					this.files.push(f);
				}
			});
		});
	}

	public getErrorMessage(element: FormControl, type: string) {
		if (element.hasError('required')) {
			return 'Este campo no puede ir vacío';
		}
		if (element.hasError('minlength')) {
			const difference = element.errors.minlength.requiredLength - element.errors.minlength.actualLength;
			return `Debe ingresar ${difference} ${difference > 1 ? `${type == 'text' ? 'caractéres' : 'números'}` : `${type === 'text' ? 'carácter' : 'número'}`} más`
		}
		if (element.hasError('maxlength')) {
			const difference = element.errors.maxlength.actualLength - element.errors.maxlength.requiredLength;
			return `Debe quitar ${difference} ${difference > 1 ? `${type == 'text' ? 'caractéres' : 'números'}` : `${type === 'text' ? 'caractér' : 'número'}`}`
		}
		if (element.hasError('email')) { return 'Debe ingresar un email válido' }
		if (element.hasError('pattern')) { return 'Debe ingresar una expresión válida';}
	}

	public handleSectorChange(event, element) {
		let object = {field: element.field, value: event.value};
		this.onChangeSelect.emit(object);
		this.formGroup.patchValue({
			[element.field]: event.value
		});
	}

	public handleImageUploaded(event: any) {
		this.hasImage = true;
	}

	public async onSubmit(event) {
		if (this.formGroup.valid) {
			this.loading = true;
			this.form.forEach(({ field, display, type }) => {
				if (display === 'datepicker') {
					const parsedObject = {};
					parsedObject[field] = this.utilService.formatDate(this.formGroup.value[field]);
					this.formGroup.patchValue(parsedObject);
				}
			});
			if (this.hasImage) {
				const { success, message, id, url } = await this.imageUploaderComponent.submit();
				if (success) {
					this.formGroup.patchValue({ image_id: id, url });
					this.imageUrl = url;
				}
			}

			if (!this.method) {
				const retValue = this.dataElement && this.dataElement.id
					? { ...this.formGroup.value, ...{ id: this.dataElement.id, url: this.imageUrl ? this.imageUrl : null } }
					: { ...this.formGroup.value, ...{ url: this.imageUrl ? this.imageUrl : null } };
				this.close(retValue);
			} else {
				switch (this.method) {
					case 'put':
						this.httpService.put(`${this.apiUrl}${this.dataElement?.id ? `${this.subUrl || '/'}${this.dataElement.id}` : ''}`, this.formGroup.value).then(({ success, message }) => {
							this.loading = false;
							if (success) {
								if(this.apiUrl==='advices'){
									this.createUploads(`advicesxUploads`, this.dataElement.id);
								}
								this.utilService.notification(message, '', 3);
								this.formGroup.reset();
								this.close({ succes: true });
							} else {
								console.error(message);
								this.errorMessage = message;
							}
						}).catch((error) => {
							this.loading = false;
							console.error(error);
						});
						break;
					case 'patch':
						this.httpService.post(`${this.apiUrl}${this.dataElement?.id ? `${this.subUrl || '/'}${this.dataElement.id}` : ''}`, this.formGroup.value).then(({ success, message }) => {
							this.loading = false;
							if (success) {
								this.utilService.notification(message, '', 3);
								this.formGroup.reset();
								this.close({ succes: true });
							} else {
								console.error(message);
								this.errorMessage = message;
							}
						}).catch((error) => {
							this.loading = false;
							console.error(error);
						});
						break;
					case 'post':
						this.httpService.post(`${this.apiUrl}${this.dataElement?.id ? `${this.subUrl || '/'}${this.dataElement.id}` : ''}`, this.formGroup.value).then(({ success, message, id }) => {
							this.loading = false;
							if (success) {
								if(this.apiUrl=='advices'){
									this.createUploads(`advicesxUploads`, id);
								}
								this.utilService.notification(message, '', 3);
								this.formGroup.reset();
								this.close({ succes: true });
							} else {
								console.error(message);
								this.errorMessage = message;
							}
						}).catch((error) => {
							this.loading = false;
							console.error(error);
						});
						break;
				}
			}
		}
	}

	public clearError(): void {
		this.errorMessage = "";
	}

	public close(result?: any): void {
		this.dialogRef.close(result);
	}

	public sanitizeHTML(element:string):SafeHtml{
		return this.sanitizer.bypassSecurityTrustHtml(element);
	}

	public checkCUIT(value:string){
		if(this.formGroup.controls.cuit){
			if(value.length==2 || value.length==11){
				let val=value;
				this.formGroup.controls.cuit.setValue(`${value}-`);
			}else{
				if(value.length>13){
					this.formGroup.controls.cuit.setValue('');
				}
			}
		}
	}

	addFileId(fileId:number){
		this.filesId.push(fileId);
		if(this.multiple){
			this.formGroup.get('upload_id').setValue(this.filesId);
		}
	}

	removeFileId(fileId:number){
		let index=this.filesId.indexOf(fileId);
		if(index!==-1){
			this.filesId.splice(index,1);
			if(this.multiple){
				this.formGroup.get('upload_id').setValue(this.filesId);
			}
		}
	}

	createUploads(route:string, id:number){
		let files:number[]=this.method==='put'?this.filesId.filter(f=>!this.uploadedFilesId.includes(f)) : this.filesId;
		files.forEach(fileId=>{
			const rowToInsert={
				'advice_id': id,
				'upload_id': fileId
			};
			this.httpService.post(route, rowToInsert).then(({success, message})=>{
				if(success){
					//this.utilService.notification(message,'',3);
				}else{
					console.error(message);
				}
			});
		});
	}

	getFile(id_oldquote:number){
		this.httpService.getPDF(`oldQuotes/preview/${id_oldquote}`).then((res:any)=>{
			var url=window.URL.createObjectURL(res);
			window.open(url,"_blank")?.focus();
		});
	}

	changeToNoActivo() {
		this.httpService.post(`${this.apiUrl}/noActivo/${this.dataElement?.id}`, { id: this.dataElement?.id }).then((res: any) => {
			if(res.message) {
				this.utilService.notification(res.message, '', 3);
			}
		},
		error => {
			this.utilService.notification(error.error, '', 3);
		});
	}

	changeToBaja() {
		this.httpService.post(`${this.apiUrl}/baja/${this.dataElement?.id}`, { id: this.dataElement?.id }).then((res: any) => {
			if(res.message) {
				this.utilService.notification(res.message, '', 3);
			}
		},
		error => {
			this.utilService.notification(error.error, '', 3);
		});
	}

	activateMatriculado() {
		this.httpService.post(`${this.apiUrl}/activate/${this.dataElement?.id}`, { id: this.dataElement?.id }).then((res: any) => {
			if(res.message) {
				this.utilService.notification(res.message, '', 3);
			}
		},
		error => {
			this.utilService.notification(error.error, '', 3);
		});
	}
}
