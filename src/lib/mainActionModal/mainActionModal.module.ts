import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainActionModalComponent } from './mainActionModal.component';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { ButtonModule } from '../button/button.module';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { ReactiveFormsModule } from '@angular/forms';
import { LoadingModule } from '../loading/loading.module';
import { ErrorAlertModule } from '../alert/error-alert/error-alert.module';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ImageUploaderModule } from '../image-uploader/image-uploader.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { FileUploaderModule } from '../file-uploader/file-uploader.module';

@NgModule({
    declarations: [
        MainActionModalComponent,
    ],
    imports: [
        CommonModule,
        ButtonModule,
        LoadingModule,
        MatDialogModule,
        MatDividerModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatIconModule,
        MatButtonModule,
        MatRippleModule,
        MatFormFieldModule,
        MatCheckboxModule,
        MatSelectModule,
        MatInputModule,
        MatRadioModule,
        ReactiveFormsModule,
        ErrorAlertModule,
        MatSlideToggleModule,
        ImageUploaderModule,
        AngularEditorModule,
				FileUploaderModule
    ],
    exports: [
        MainActionModalComponent
    ],
    providers: [
        MatDatepickerModule,
        {provide: MatDialogRef, useValue: {}},
    ]
})
export class MainActionModalModule { }
